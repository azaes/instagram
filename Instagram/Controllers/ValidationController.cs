﻿using System.Linq;
using Instagram.Models;
using Microsoft.AspNetCore.Mvc;

namespace Instagram.Controllers
{
    
    public class ValidationController : Controller
    {
        private InstagramContext context;


        public ValidationController(InstagramContext context)
        {
            this.context = context;
        }

        [AcceptVerbs("Get", "Post")]
        public IActionResult CheckLogin(string login)
        {
            User user = context.Users.FirstOrDefault(u => u.Login == login);
            return Json(user == null);
        }
    }
}