﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Instagram.Models;
using Instagram.Services;
using Instagram.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Instagram.Controllers
{
    public class PostController : Controller
    {
        private InstagramContext context;
        private FileUploadService fileUploadService;
        private IHostingEnvironment environment;

        public PostController(InstagramContext context, 
            FileUploadService fileUploadService,
            IHostingEnvironment environment)
        {
            this.context = context;
            this.fileUploadService = fileUploadService;
            this.environment = environment;
        }

        [Authorize]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        public IActionResult Create(CreatePostModel model)
        {
            User user = context.Users.FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            Post post = new Post()
            {
                Title = model.Title,
                UserId = user.Id,
                PostDate = DateTime.Now,
                PostImagePath = $"images/{user.Login}/{model.PostImage.FileName}"
            };

            context.Posts.Add(post);

            string path = Path.Combine(
                environment.WebRootPath,
                $"images/{user.Login}_{model.PostImage.FileName}");

            fileUploadService.Upload(path, model.PostImage.FileName, model.PostImage);
            user.PostCounter++;
            context.SaveChanges();

            return RedirectToAction("Index", "Home");
        }
        
        
        [HttpPost]
        public IActionResult Like(int postId)
        {
            User user = context.Users.Include(u => u.Posts).FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            Post post = context.Posts.FirstOrDefault(p => p.Id == postId);

            context.Likes.Add(new Like()
            {
                LikeDate = DateTime.Now,
                PostId = postId,
                User = user
            });
            
            post.Likes++;
            context.SaveChanges();
            return Json(post.Likes);

//            return RedirectToAction("Index", "Home");
        }
        
        [HttpPost]
        public IActionResult DisLike(int postId)
        {
            User user = context.Users.Include(u => u.Posts).FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            Post post = context.Posts.FirstOrDefault(p => p.Id == postId);
            if (post.Likes != null)
            {
                Like like = context.Likes.FirstOrDefault(l => l.PostId == post.Id && l.UserId == user.Id);
                context.Likes.Remove(like);
                
                post.Likes --;
            }
            context.SaveChanges();
            
            return Json(post.Likes);
//            return RedirectToAction("Index", "Home");
        }
        
        
        
        public IActionResult Details(int id)
        {
            User user = context.Users.Include(u => u.Posts)
                .FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            Post post = context.Posts.FirstOrDefault(p => p.Id == id);
            List<Comment> comments = new List<Comment>();
            PostDetailsViewModel model = new PostDetailsViewModel()
            {
                User = user,
                Post = post,
                
                CommentsViewModel = new CommentsViewModel()
                {
                    UserId = user.Id,
                    PostId = post.Id,
                    Comments = comments.OrderByDescending(p => p.CommentDate).ToList()
                }
           
            return View(model);
        }
        
        
        [HttpPost]
        public IActionResult Comment(int postId, string userId, string text)
        {
            User user = context.Users.Include(u => u.Posts).FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            Post post = context.Posts.FirstOrDefault(p => p.Id == postId);
            Comment comment = new Comment() {Text = text, CommentDate = DateTime.Now, UserId = user.Id, PostId = post.Id};
            context.Comments.Add(comment);
            
            context.SaveChanges();

            return Json(comment.Text);
//            return RedirectToAction("Index", "Home");
        }
        
        [HttpPost]
        public IActionResult Edit(int postId)
        {
            User user = context.Users.Include(u => u.Posts)
                .FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            Post post = context.Posts.FirstOrDefault(p => p.Id == postId);
            context.Posts.Update(post);
            context.SaveChanges();
            return Json(post.Title);
//            return RedirectToAction("Index", "Home", new {post.Id});
        }
    }
}