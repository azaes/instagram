﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Instagram.Models;
using Instagram.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Instagram.Controllers
{
    public class HomeController : Controller
    {
        private InstagramContext context;

        
        
        public HomeController(InstagramContext context)
        {
            this.context = context;
            
        }
        
        [Authorize]
        public IActionResult Index()
        {
            

            User user = context.Users.Include(u => u.Posts).ThenInclude(c => c.Comments).FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            IQueryable<Subscription> userSubscriptions = context.Subscriptions.Where(s => s.SubscriberId == user.Id);
//            List<Post> posts = context.Posts.Include(c => c.Comments).ToList();
            List<Post> posts = new List<Post>();
            foreach (Subscription s in userSubscriptions)
            {
                User follower = context.Users.Include(p => p.Posts).ThenInclude(c => c.Comments).FirstOrDefault(u => u.Id == s.ToSubscribeId);
                posts.AddRange(follower.Posts);
            }
            posts.AddRange(user.Posts);

            IndexModel model = new IndexModel()
            {
                User = user,
                PostsFeed = posts.OrderByDescending(p => p.PostDate).ToList(),
            };

            return View(model);
        }

        public IActionResult Profile(int userId)
        {
            User user = context.Users.Include(u => u.Posts).FirstOrDefault(u => u.Id == userId );
            User viewUser = context.Users.FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            ProfileModel model = new ProfileModel()
            {
                User = user,
                ViewerUser = viewUser,
                isSubscribed = context.Subscriptions.FirstOrDefault(
                    s => s.SubscriberId == viewUser.Id && s.ToSubscribeId == user.Id) != null
            };

            return View(model);
        }

//        public IActionResult Search(string key)
//        {
//            List<User> users = context.Users.ToList();
//            if (!string.IsNullOrEmpty(key))
//            {
//                users = users.Where(u => 
//                    u.Login.Contains(key) || u.Name.Contains(key) || u.Email.Contains(key)).ToList();
//            }
//
//            return View(users);
//        }
        public IActionResult SearchAjax()
        {
            return View();
        }

        public IActionResult SearchAjaxResult(string keyWord)
        {
            List<User> users = context.Users.ToList();
                
            if (!string.IsNullOrEmpty(keyWord))
            {
               users = context.Users.Where(u =>
                    u.Name.Contains(keyWord) ||
                    u.Email.Contains(keyWord)).ToList();
            }

            return PartialView("UserSearchResult", users);
        }
        
        


    }
}