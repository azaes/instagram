﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Instagram.Models;
using Instagram.Services;
using Instagram.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;

namespace Instagram.Controllers
{
    public class AccountController : Controller
    {
            private InstagramContext db;
            private IHostingEnvironment environment;
            private FileUploadService fileUploadService;
            private readonly IStringLocalizer<HomeController> _localizer;

            public AccountController(
                InstagramContext context,
                IHostingEnvironment environment,
                FileUploadService fileUploadService,
                IStringLocalizer<HomeController> localizer
            )
            {
                db = context;
                this.environment = environment;
                this.fileUploadService = fileUploadService;
                _localizer = localizer;
            }

            [HttpGet]
            public IActionResult Login()
            {
                ViewData["Title"] = _localizer["Header"];
                ViewData["Message"] = _localizer["Message"];
                
                return View();
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> Login(LoginModel model)
            {
                if (ModelState.IsValid)
                {
                    User user = await db.Users.FirstOrDefaultAsync(u =>
                        u.Login == model.Login && u.Password == model.Password);
                    if (user != null)
                    {
                        await Authenticate(user); // аутентификация

                        return RedirectToAction("Index", "Home");
                    }

                    ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }

                return View(model);
            }

            [HttpGet]
            public IActionResult Register()
            {
                return View();
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public async Task<IActionResult> Register(RegisterModel model)
            {
                if (ModelState.IsValid)
                {
                    User user = await db.Users.FirstOrDefaultAsync(u => u.Login == model.Login);

                    string path = Path.Combine(
                        environment.WebRootPath,
                        $"images\\{model.Login}\\avatar");

                    fileUploadService.Upload(path, model.AvatarImage.FileName, model.AvatarImage);


                    if (user == null)
                    {
                        db.Users.Add(new User
                        {
                            Login = model.Login,
                            Email = model.Email ?? string.Empty,
                            Gender = model.Gender ?? string.Empty,
                            Info = model.Info ?? string.Empty,
                            Name = model.Name,
                            Phone = model.Phone ?? string.Empty,
                            Password = model.Password,
                            AvatarImage = $"images/{model.Login}/avatar/{model.AvatarImage.FileName}"

                        });
                        await db.SaveChangesAsync();

                        await Authenticate(db.Users.FirstOrDefault(u => u.Login == model.Login));

                        return RedirectToAction("Index", "Home");
                    }
                    else
                        ModelState.AddModelError("", "Некорректные логин и(или) пароль");
                }

                return View(model);
            }

            private async Task Authenticate(User user)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login)
                };
                ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
                    ClaimsIdentity.DefaultRoleClaimType);
                await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme,
                    new ClaimsPrincipal(id));
            }

            public async Task<IActionResult> Logout()
            {
                await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
                return RedirectToAction("Login", "Account");
            }
        
            public string GetCulture(string code = "")
            {
                if (!string.IsNullOrEmpty(code))
                {
                    CultureInfo.CurrentCulture = new CultureInfo(code);
                    CultureInfo.CurrentUICulture = new CultureInfo(code);
                }
    
                return $"CurrentCulture: {CultureInfo.CurrentCulture.Name}, {CultureInfo.CurrentUICulture.Name}";
            }
            
            [HttpPost]
            public IActionResult SetLanguage(string culture, string returnUrl)
            {
                Response.Cookies.Append(
                    CookieRequestCultureProvider.DefaultCookieName,
                    CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                    new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
                );
    
                return LocalRedirect(returnUrl);
            }
        }
    }
