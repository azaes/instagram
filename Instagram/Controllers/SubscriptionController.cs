﻿using System.IO;
using System.Linq;
using Instagram.Models;
using Instagram.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Instagram.Controllers
{
    public class SubscriptionController : Controller
    {
        private InstagramContext context;


        public SubscriptionController(InstagramContext context)
        {
            this.context = context;
        }

        
        public string Subscribe(string currentUserLogin, int followersId)
        {
            User user = context.Users.FirstOrDefault(u => u.Login == currentUserLogin);
            User followers = context.Users.FirstOrDefault(u => u.Id == followersId);
            Subscription subscription = new Subscription()
            {
                SubscriberId = user.Id,
                ToSubscribeId = followersId
            };
            context.Subscriptions.Add(subscription);
            user.Subscriptions++;
            followers.Subscribers++;

            int subscriptions = followers.Subscriptions;
            int subscribers = followers.Subscribers;
            
            context.SaveChanges();
            
            string json2 = "{\"subscriptions\":" + $"{subscriptions}," +  "\"subscribers\":" + $"{subscribers}" + "}";

            string result = "'{" + $"'subscriptions': {subscriptions}, 'subscribers': {subscribers}" + "}'";
            
            var json = JsonConvert.SerializeObject(result);
            return json2;
//            return RedirectToAction("Profile", "Home", new {userId =followers.Id});
        }
        [HttpPost]
        public string UnSubscribe(string currentUserLogin, int followersId)
        {
           
            
            User user = context.Users.FirstOrDefault(u => u.Login == currentUserLogin);
            User followers = context.Users.FirstOrDefault(u => u.Id == followersId);
            Subscription subscription = context.Subscriptions.FirstOrDefault(
                s => s.SubscriberId == user.Id && s.ToSubscribeId == followers.Id
                     );
//            subscription =
//                context.Subscriptions.FirstOrDefault(c => c.SubscriberId == user.Id && c.ToSubscribeId == followers.Id);
            if (subscription != null)
            {
                context.Subscriptions.Remove(subscription);
                user.Subscriptions--;
                followers.Subscribers--;
                context.SaveChanges();
            }
            
//            ProfileModel model = new ProfileModel()
//            {
//                User = user
//            };
            
            int subscriptions = followers.Subscriptions;
            int subscribers = followers.Subscribers;
            string json2 = "{\"subscriptions\":" + $"{subscriptions}," +  "\"subscribers\":" + $"{subscribers}" + "}";
            var json = JsonConvert.SerializeObject("{" + $"'subscriptions': {subscriptions}, 'subscribers': {subscribers}" + "}");
//            return PartialView("../Home/UsersList", model);
//            return Json(new { Url = Url.Action("../Home/UsersList", model) });
//            return Json(new { error = true, message = RenderViewToString(PartialView("Evil", model))});
            return json2;
//            return RedirectToAction("Profile", "Home", new {userId =followers.Id});
        }
        
        
     
    }
}