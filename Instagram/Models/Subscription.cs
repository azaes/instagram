﻿using System.ComponentModel.DataAnnotations;
using System.Data.Common;

namespace Instagram.Models
{
    public class Subscription
    {
        [Key]
        public int Id { get; set; } 
        
        public int SubscriberId { get; set; } //kto podpisalsya
        public int ToSubscribeId { get; set; } //na kogo podpisalsya
        
    }
}