﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Instagram.Models
{
    public class Like
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int PostId { get; set; }
        public DateTime LikeDate { get; set; }
        
    }
}