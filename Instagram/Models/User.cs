﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Instagram.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        
        public string Login { get; set; }        
        public string Email { get; set; }
        public string AvatarImage { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        
        public int PostCounter { get; set; }
        public int Subscriptions { get; set; }
        public  int Subscribers { get; set; }
        
        public List<Post> Posts { get; set; }

        public User()
        {
            Posts = new List<Post>();
        }
    }
}