﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Instagram.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Text { get; set; }
        
        public DateTime CommentDate { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }
        public int PostId { get; set; }
        public Post Post { get; set; }
    }
}