﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;

namespace Instagram.Models
{
    public class Post
    {
        public int Id { get; set; }
        
        public string Title { get; set; }
        
        public string PostImagePath { get; set; }
        
        public DateTime PostDate { get; set; }
                
        public int Likes { get; set; }
        
        public int UserId { get; set; }
        public User User { get; set; }
        
        public List<Comment> Comments { get; set; }
    }
}