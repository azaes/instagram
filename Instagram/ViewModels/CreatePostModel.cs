﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Instagram.ViewModels
{
    public class CreatePostModel
    {
        [Required]
        public string Title { get; set; }
        
        [Required]
        public IFormFile PostImage { get; set; }
    }
}