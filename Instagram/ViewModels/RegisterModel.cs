﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Instagram.ViewModels
{
    public class RegisterModel
    {
        [Required]
        [Remote("CheckLogin", "Validation", "Login already exist")]
        public string Login { get; set; }
        
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        
        [Required]
        public IFormFile AvatarImage { get; set; }
        
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        
        [Required]
        [Compare("Password", ErrorMessage = "Password doesn't coincide")]
        [DataType(DataType.Password)]
        public string PasswordConfirm { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
    }
}