﻿using Instagram.Models;

namespace Instagram.ViewModels
{
    public class ProfileModel
    {
        public User User { get; set; }
        public User ViewerUser { get; set; }
        public bool isSubscribed { get; set; }
    }
}