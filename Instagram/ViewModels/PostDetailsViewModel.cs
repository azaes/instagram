﻿using System.Collections.Generic;
using Instagram.Models;

namespace Instagram.ViewModels
{
    public class PostDetailsViewModel
    {
        public User User { get; set; }
        public Post Post { get; set; }
        public CommentsViewModel CommentsViewModel { get; set; }
    }
}