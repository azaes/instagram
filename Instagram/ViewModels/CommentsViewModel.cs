﻿using System.Collections.Generic;
using Instagram.Models;

namespace Instagram.ViewModels
{
    public class CommentsViewModel
    {
        public User User { get; set; }
        public  int UserId { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
        public List<Comment> Comments { get; set; }
    }
}