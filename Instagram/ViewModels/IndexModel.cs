﻿using System.Collections.Generic;
using Instagram.Models;

namespace Instagram.ViewModels
{
    public class IndexModel
    {
        public User User { get; set; }
        public List<Post> PostsFeed { get; set; }
    }
}